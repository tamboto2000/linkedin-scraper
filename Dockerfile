# Start from the latest golang base image
FROM ubuntu:latest

# Add Maintainer Info
LABEL maintainer="Franklin Collin Tamboto <tamboto2000@gmail.com>"

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy files to working directory
COPY app/app ./

#Install SSL-CA
RUN apt-get update
RUN apt-get install -y ca-certificates

# Expose port 8080 to the outside world
EXPOSE 8000

# Command to run the executable
CMD ["./app"]
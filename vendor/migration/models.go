package migration

import (
	"github.com/jinzhu/gorm"
)

//Migrate migrate all models to table
func Migrate(db *gorm.DB, models ...interface{}) error {
	return db.AutoMigrate(models).Error
}

//RecreateTable recreate tables in database db
func RecreateTable(db *gorm.DB, models ...interface{}) error {
	err := db.DropTableIfExists(models).Error

	if err != nil {
		return err
	}

	err = db.CreateTable(models).Error

	return err
}

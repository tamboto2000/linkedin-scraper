package controller

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type Context struct {
	App     *Controller
	Writer  http.ResponseWriter
	Request *http.Request
	Log     *log.Log
}

func (ctx *Context) InternalError(err string) {
	response := Response{
		Status:     "internal server error",
		StatusCode: 500,
		Message:    err,
	}

	ctx.Writer.WriteHeader(500)
	json.NewEncoder(ctx.Writer).Encode(response)
}

func (ctx *Context) Forbidden(err string) {
	response := Response{
		Status:     "forbidden",
		StatusCode: 403,
		Message:    err,
	}

	ctx.Writer.WriteHeader(403)
	json.NewEncoder(ctx.Writer).Encode(response)
}

func (ctx *Context) NotFound(err string) {
	response := Response{
		Status:     "not found",
		StatusCode: 404,
		Message:    err,
	}

	ctx.Writer.WriteHeader(404)
	json.NewEncoder(ctx.Writer).Encode(response)
}

func (ctx *Context) BadRequest(err string) {
	response := Response{
		Status:     "bad request",
		StatusCode: 400,
		Message:    err,
	}

	ctx.Writer.WriteHeader(400)
	json.NewEncoder(ctx.Writer).Encode(response)
}

func (ctx *Context) RequestOK(msg string) {
	response := Response{
		Status:     "OK",
		StatusCode: 200,
		Message:    msg,
	}

	ctx.Writer.WriteHeader(200)
	json.NewEncoder(ctx.Writer).Encode(response)
}

func (ctx *Context) ServeContent(fname string, data []byte) {
	bytesReader := bytes.NewReader(data)
	modTime := time.Now()

	ctx.Writer.Header().Set("Content-Disposition", "attachment; filename="+fname)

	http.ServeContent(ctx.Writer, ctx.Request, fname, modTime, bytesReader)
}

func (ctx *Context) WriteJSON(data interface{}) error {
	return json.NewEncoder(ctx.Writer).Encode(data)
}

func (ctx *Context) Write(data []byte) (int, error) {
	len, err := ctx.Writer.Write(data)
	return len, err
}

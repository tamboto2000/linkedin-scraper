package application

import (
	"controller"
	"fmt"
	"log"
	"router"
	"worker"

	"github.com/jinzhu/gorm"
)

type App struct {
	DB         *gorm.DB
	Controller *controller.Controller
	Worker     *worker.Pool
	Router     *router.Router
	Log        *log.Log
}

func (app *App) AddDB(username, password, host, port, dbname string) error {
	connArgs := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbname)
	db, err := gorm.Open("mysql", connArgs)
	if err != nil {
		return err
	}

	app.DB = db

	return nil
}

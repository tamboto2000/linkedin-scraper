package worker

import (
	"errors"
	"log"
	"sync"
)

//Worker is an interface for concurrent processes
type Worker interface {
	//Run run the worker concurrently
	Run() error

	//Wait wait the worker to return
	Wait()

	//ReadLog read log coming from worker, index 0 is tag, index 1 is the log text
	ReadLog() chan [2]string

	//TerminateOnFinish decide if worker will terminate after finish it's job
	//or stayed in pool
	TerminateOnFinish() bool

	//CleanUp clean every variable, buffer, or any footprint that left from previous
	//job to reduce memory footprint
	CleanUp() error
}

//Pool is a "pool" for Worker(s)
type Pool struct {
	Log     *log.Log
	workers *sync.Map
}

//NewPool create new worker pool
func NewPool() *Pool {
	return &Pool{
		workers: new(sync.Map),
	}
}

//Load load worker outside off pool
func (pool *Pool) Load(name string) (interface{}, bool) {
	return pool.workers.Load(name)
}

//Add add worker to pool
func (pool *Pool) Add(name string, wk Worker) error {
	_, ok := pool.workers.Load(name)
	if ok {
		return errors.New("worker already exist")
	}

	pool.workers.Store(name, wk)

	return nil
}

//Run run function Worker.Run()
func (pool *Pool) Run(name string) error {
	wk, ok := pool.workers.Load(name)
	if !ok {
		return errors.New("worker not exist")
	}

	err := wk.(Worker).Run()
	if err != nil {
		return err
	}

	exitChan := make(chan bool)
	go pool.readLog(name, exitChan)
	go pool.wait(name, exitChan)

	return nil
}

func (pool *Pool) wait(name string, exit chan bool) {
	wk, _ := pool.workers.Load(name)

	wk.(Worker).Wait()
	exit <- true

	wk.(Worker).CleanUp()

	if wk.(Worker).TerminateOnFinish() {
		pool.workers.Delete(name)
	}
}

func (pool *Pool) storeLog(tag, text string) {
	pool.Log.Write(tag, text)
}

func (pool *Pool) readLog(name string, exit chan bool) {
	worker, _ := pool.workers.Load(name)
	logChan := worker.(Worker).ReadLog()

	for {
		select {
		case logCont := <-logChan:
			if len(logCont) == 0 {
				continue
			}

			if len(logCont) < 2 {
				pool.storeLog(name, logCont[0])
				continue
			}

			pool.storeLog(logCont[0], logCont[1])

		case <-exit:
			close(exit)
			return
		}
	}
}

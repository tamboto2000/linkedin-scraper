package main

import (
	"linkedin_scraper/app/models"

	"github.com/jinzhu/gorm"
)

func migrate(db *gorm.DB) {
	db.AutoMigrate(
		models.LinkedinContactAdvanced{},
		models.LinkedinContactGeneral{},
		models.LinkedinJob{},
		models.LinkedinProfileGeneral{},
		models.LinkedinResult{},
		models.LinkedinSchool{},
		models.LinkedinSkill{},
	)
}

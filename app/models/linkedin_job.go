package models

import "time"

type LinkedinJob struct {
	ID           int    `gorm:"AUTO_INCREMENT"`
	UserID       string `gorm:"size:255"`
	Address      string `gorm:"mediumtext"`
	LocationName string `gorm:"mediumtext"`
	CountryCode  string
	PostalCode   string
	CompanyName  string
	CompanyURL   string `gorm:"mediumtext"`
	JobTitle     string
	FromYear     int
	FromMonth    int
	ToYear       int
	ToMonth      int
	Description  string    `gorm:"mediumtext"`
	CreatedAt    time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt    time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

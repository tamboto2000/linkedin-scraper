package models

import "time"

type LinkedinSchool struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	UserID    string `gorm:"size:255"`
	URL       string `gorm:"type:mediumtext"`
	Logo      string `gorm:"type:mediumtext"`
	Name      string `gorm:"mediumtext"`
	DateRange string
	Degree    string
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

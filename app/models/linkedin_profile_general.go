package models

import "time"

type LinkedinProfileGeneral struct {
	ID          int    `gorm:"AUTO_INCREMENT"`
	UserID      string `gorm:"type:mediumtext"`
	Name        string
	ProfilePict string `gorm:"type:mediumtext"`
	Summary     string `gorm:"type:mediumtext"`
	HeadLine    string `gorm:"type:mediumtext"`
	Connection  int
	Follower    int
	Address     string `gorm:"type:mediumtext"`
	PlaceName   string `gorm:"type:mediumtext"`
	CountryCode string
	PostalCode  string
	CreatedAt   time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt   time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

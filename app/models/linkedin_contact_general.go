package models

import "time"

type LinkedinContactGeneral struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	UserID    string `gorm:"size:255"`
	Email     string
	Address   string `gorm:"type:mediumtext"`
	DOB       string
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

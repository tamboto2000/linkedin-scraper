package models

import "time"

type LinkedinSkill struct {
	ID               int    `gorm:"AUTO_INCREMENT"`
	UserID           string `gorm:"size:255"`
	Name             string
	EndorsementCount string
	CreatedAt        time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt        time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

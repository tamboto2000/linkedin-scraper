package models

import "time"

type LinkedinContactAdvanced struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	UserID    string `gorm:"size:255"`
	Type      string
	Data      string    `gorm:"type:mediumtext"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

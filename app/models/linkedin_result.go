package models

import "time"

type LinkedinResult struct {
	ID        int `gorm:"AUTO_INCREMENT"`
	Type      string
	UserID    string    `gorm:"size:255"`
	ReqID     string    `gorm:"size:255"`
	Name      string    `gorm:"size:255"`
	Data      string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

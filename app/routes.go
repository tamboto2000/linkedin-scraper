package main

import (
	"router"
)

var routes = router.NewRouter()

//Routes register routes
func Routes() *router.Router {
	routes.AddFunc("/scrape", ctrl.Controller("startScraper"), "POST")
	routes.AddFunc("/download", ctrl.Controller("download"), "GET")

	return routes
}

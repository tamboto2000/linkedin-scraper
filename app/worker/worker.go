package worker

import (
	"api-template/app/worker/hello"
	"worker"
)

var pool = worker.NewPool()

//Pool return pool for workers
func Pool() *worker.Pool {
	pool.Add("ticker", hello.New())

	return pool
}

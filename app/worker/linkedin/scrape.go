package linkedin

import (
	"collin/spider/linkedin"
	"linkedin_scraper/app/misc/reporter"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
)

type Result struct {
	ResultMap *sync.Map `json:"-"`
	Result    map[string]linkedin.Profile
}

type scrapeWorker struct {
	db          *gorm.DB
	userID      string
	reqID       string
	lable       string
	csvData     []map[string]string
	usernamCol  string
	uniqueIDCol string
	sessCookie  string
	result      Result
	reporter    *reporter.Reporter
	wg          *sync.WaitGroup
	log         chan [2]string
}

func (wk *scrapeWorker) run() {
	wk.result.ResultMap = new(sync.Map)
	wk.result.Result = make(map[string]linkedin.Profile)
	wk.divideAndRun()

	wk.wg.Wait()
}

func (wk *scrapeWorker) divideAndRun() {
	count := 20
	accounts := []map[string]string{}
	for _, row := range wk.csvData {
		count--
		accounts = append(accounts, row)
		if count == 0 {
			wk.wg.Add(1)
			wk.scrape(accounts)
			count = 20
			accounts = []map[string]string{}
		}
	}

	if len(accounts) > 0 {
		wk.wg.Add(1)
		wk.scrape(accounts)
	}
}

func (wk *scrapeWorker) scrape(accounts []map[string]string) {
	cl, err := linkedin.NewClient(wk.sessCookie)
	if err != nil {
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID}
	}
	profServ := cl.NewProfileService()

	for _, row := range accounts {
		username := row[wk.usernamCol]
		uniqueID := row[wk.uniqueIDCol]

		wk.reporter.Write("info", "Scraping account "+uniqueID)

		profile, err := profServ.GetProfile(username)
		if err != nil {
			wk.log <- [2]string{"linkedin_scraper_" + wk.reqID}
			go wk.reporter.Write("error", "Error on account "+uniqueID+" "+err.Error())
			continue
		}

		wk.result.ResultMap.Store(uniqueID, profile)

		wk.reporter.Write("info", "Account "+uniqueID+" scraped")
		time.Sleep(1 * time.Second)
	}

	wk.wg.Done()
}

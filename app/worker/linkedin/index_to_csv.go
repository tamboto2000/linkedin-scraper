package linkedin

import (
	"bytes"
	"collin/spider/linkedin"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"instagram_scraper/app/misc/ziper"
	"linkedin_scraper/app/misc/reporter"
	"linkedin_scraper/app/models"
	"strconv"
	"sync"

	"github.com/mohae/struct2csv"

	"github.com/jinzhu/gorm"
)

type indexToCSVWorker struct {
	db          *gorm.DB
	userID      string
	reqID       string
	lable       string
	usernamCol  string
	uniqueIDCol string
	reporter    *reporter.Reporter
	sessCookie  string
	result      *Result
	csvMap      *sync.Map
	wg          *sync.WaitGroup
	log         chan [2]string
}

func (wk *indexToCSVWorker) run() {
	wk.wg.Add(6)
	go wk.indexProfileGeneral()
	go wk.indexJob()
	go wk.indexSkill()
	go wk.indexSchool()
	go wk.indexContactGeneral()
	go wk.indexAdvancedContact()
	wk.wg.Wait()

	compressed, err := wk.compressCSVToZIP()
	if err != nil {
		fmt.Println(err.Error())
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID, err.Error()}
		return
	}

	encoded := encodeToBase64(compressed)
	if err := wk.db.Create(&models.LinkedinResult{
		Type:   "csv",
		UserID: wk.userID,
		ReqID:  wk.reqID,
		Name:   "result.zip",
		Data:   encoded,
	}).Error; err != nil {
		fmt.Println(err.Error())
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID, err.Error()}
	}
}

func (wk *indexToCSVWorker) indexProfileGeneral() {
	profiles := []models.LinkedinProfileGeneral{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		appendProfile := models.LinkedinProfileGeneral{
			UserID:      key.(string),
			Name:        prepProfile.Name,
			ProfilePict: prepProfile.ProfilePict,
			Summary:     prepProfile.Summary,
			HeadLine:    prepProfile.HeadLine,
		}

		if prepProfile.Connection != nil {
			appendProfile.Connection = prepProfile.Connection.Connection
			appendProfile.Follower = prepProfile.Connection.Follower
		}

		if prepProfile.Location != nil {
			appendProfile.Address = prepProfile.Location.FullAddress
			appendProfile.PlaceName = prepProfile.Location.Name
			appendProfile.CountryCode = prepProfile.Location.CountryCode
			appendProfile.PostalCode = prepProfile.Location.PostalCode
		}

		profiles = append(profiles, appendProfile)

		return true
	})

	wk.csvMap.Store("profile_general.csv", profiles)

	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexJob() {
	jobs := []models.LinkedinJob{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		if prepProfile.Job != nil {
			for _, job := range *prepProfile.Job {
				appndJob := models.LinkedinJob{
					UserID:      key.(string),
					CompanyName: job.CompanyName,
					CompanyURL:  job.CompanyURL,
					JobTitle:    job.JobTitle,
					FromYear:    job.DateRange.FromYear,
					FromMonth:   job.DateRange.FromMonth,
					ToYear:      job.DateRange.ToYear,
					ToMonth:     job.DateRange.ToMonth,
					Description: job.Description,
				}

				if job.Location != nil {
					appndJob.Address = job.Location.FullAddress
					appndJob.LocationName = job.Location.Name
					appndJob.CountryCode = job.Location.CountryCode
					appndJob.PostalCode = job.Location.PostalCode
				}

				jobs = append(jobs, appndJob)
			}
		}

		return true
	})

	wk.csvMap.Store("job.csv", jobs)

	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexSkill() {
	skills := []models.LinkedinSkill{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		if prepProfile.Skill != nil {
			for _, skill := range *prepProfile.Skill {
				skills = append(skills, models.LinkedinSkill{
					UserID:           key.(string),
					Name:             skill.Name,
					EndorsementCount: skill.EndorsementCount,
				})
			}
		}

		return true
	})

	wk.csvMap.Store("skill.csv", skills)
	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexSchool() {
	schools := []models.LinkedinSchool{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		if prepProfile.School != nil {
			for _, school := range *prepProfile.School {
				schools = append(schools, models.LinkedinSchool{
					UserID:    key.(string),
					URL:       school.URL,
					Logo:      school.Logo,
					Name:      school.Name,
					DateRange: school.DateRange,
					Degree:    school.Degree,
				})
			}
		}

		return true
	})

	wk.csvMap.Store("school.csv", schools)
	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexContactGeneral() {
	contacts := []models.LinkedinContactGeneral{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		if prepProfile.Contact != nil {
			contact := models.LinkedinContactGeneral{
				UserID:  key.(string),
				Email:   prepProfile.Contact.Email,
				Address: prepProfile.Contact.Address,
			}

			if prepProfile.Contact.DOB != nil {
				year := strconv.Itoa(prepProfile.Contact.DOB.Year)
				month := strconv.Itoa(prepProfile.Contact.DOB.Month)
				day := strconv.Itoa(prepProfile.Contact.DOB.Day)

				contact.DOB = year + "-" + month + "-" + day
			}

			contacts = append(contacts, contact)
		}

		return true
	})

	wk.csvMap.Store("contact_general.csv", contacts)

	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexAdvancedContact() {
	contacts := []models.LinkedinContactAdvanced{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		prepProfile := val.(linkedin.Profile)
		if prepProfile.Contact != nil {
			if len(prepProfile.Contact.Twitter) > 0 {
				for _, twt := range prepProfile.Contact.Twitter {
					contacts = append(contacts, models.LinkedinContactAdvanced{
						UserID: key.(string),
						Type:   "twitter",
						Data:   twt,
					})
				}
			}

			if prepProfile.Contact.Websites != nil {
				for _, msc := range *prepProfile.Contact.Websites {
					contacts = append(contacts, models.LinkedinContactAdvanced{
						UserID: key.(string),
						Type:   msc.Type,
						Data:   msc.URL,
					})
				}
			}
		}
		return true
	})

	wk.csvMap.Store("contact_advanced.csv", contacts)

	wk.wg.Done()
}

func (wk *indexToCSVWorker) compressCSVToZIP() ([]byte, error) {
	zipWriter := ziper.NewZiper()

	wk.csvMap.Range(func(key, val interface{}) bool {
		//Convert to csv array
		structToCSV := struct2csv.New()
		csvArray, err := structToCSV.Marshal(val)
		if err != nil {
			fmt.Println(err.Error())
			return true
		}

		//Convert arrays to csv string
		str := ""
		buff := bytes.NewBufferString(str)
		writer := csv.NewWriter(buff)
		err = writer.WriteAll(csvArray)
		if err != nil {
			zipWriter.Close()
			fmt.Println(err.Error())
			return true
		}

		zipWriter.Add(key.(string), buff.String())

		return true
	})

	err := zipWriter.Close()
	if err != nil {
		return []byte{}, err
	}

	return zipWriter.Bytes(), nil
}

func encodeToBase64(data []byte) string {
	return base64.StdEncoding.EncodeToString(data)
}

package linkedin

import (
	"bytes"
	"collin/spider/linkedin"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"linkedin_scraper/app/models"
	"sync"

	"github.com/jinzhu/gorm"
)

type indexToJSONWorker struct {
	db          *gorm.DB
	userID      string
	reqID       string
	lable       string
	usernamCol  string
	uniqueIDCol string
	sessCookie  string
	result      *Result
	csvMap      *sync.Map
	wg          *sync.WaitGroup
	log         chan [2]string
}

func (wk *indexToJSONWorker) run() {
	jsonBytes, err := wk.normalize()
	if err != nil {
		fmt.Println(err.Error())
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID, err.Error()}
		return
	}

	compressed, err := compressAndEncode(jsonBytes)
	if err != nil {
		fmt.Println(err.Error())
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID, err.Error()}
		return
	}

	if err := wk.db.Create(&models.LinkedinResult{
		UserID: wk.userID,
		Type:   "json",
		ReqID:  wk.reqID,
		Name:   "result.json",
		Data:   compressed,
	}).Error; err != nil {
		fmt.Println(err.Error())
		wk.log <- [2]string{"linkedin_scraper_" + wk.reqID, err.Error()}
	}
}

func compressAndEncode(data []byte) (string, error) {
	buffer := bytes.NewBuffer([]byte{})
	gzWriter, err := gzip.NewWriterLevel(buffer, gzip.BestCompression)
	if err != nil {
		return "", err
	}

	if _, err := gzWriter.Write(data); err != nil {
		return "", err
	}

	if err := gzWriter.Close(); err != nil {
		return "", err
	}

	enc := base64.StdEncoding.EncodeToString(buffer.Bytes())
	return enc, nil
}

func (wk *indexToJSONWorker) normalize() ([]byte, error) {
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		wk.result.Result[key.(string)] = val.(linkedin.Profile)

		return true
	})

	return json.Marshal(wk.result)
}

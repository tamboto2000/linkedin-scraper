package linkedin

import (
	"linkedin_scraper/app/misc/reporter"
	"sync"

	"github.com/jinzhu/gorm"
)

type LinkedinScraper struct {
	DB               *gorm.DB
	UserID           string
	ReqID            string
	Lable            string
	CSVData          []map[string]string
	UsernamCol       string
	UniqueIDCol      string
	SessCookie       string
	Result           Result
	Reporter         *reporter.Reporter
	scrapeWorker     *scrapeWorker
	indexToCSVWorker *indexToCSVWorker
	wg               *sync.WaitGroup
	log              chan [2]string
	exit             chan bool
}

func New() *LinkedinScraper {
	return &LinkedinScraper{
		wg:  new(sync.WaitGroup),
		log: make(chan [2]string),
	}
}

func (wk *LinkedinScraper) Run() error {
	go func() {
		wk.Reporter.Write("info", "Start scraping")
		wk.runScrapeWorker()
		wk.Reporter.Write("info", "Start indexing")

		wk.runIndexToCSVWorker()
		wk.runIndexToJSONWorker()

		wk.Reporter.Write("finish", "finish")
		wk.exit <- true
	}()

	return nil
}

func (wk *LinkedinScraper) runScrapeWorker() {
	worker := &scrapeWorker{
		db:          wk.DB,
		userID:      wk.UserID,
		reqID:       wk.ReqID,
		csvData:     wk.CSVData,
		usernamCol:  wk.UsernamCol,
		uniqueIDCol: wk.UniqueIDCol,
		sessCookie:  wk.SessCookie,
		reporter:    wk.Reporter,
		wg:          new(sync.WaitGroup),
		log:         wk.log,
	}

	wk.scrapeWorker = worker
	worker.run()
}

func (wk *LinkedinScraper) runIndexToCSVWorker() {
	worker := &indexToCSVWorker{
		db:          wk.DB,
		userID:      wk.UserID,
		reqID:       wk.ReqID,
		usernamCol:  wk.UsernamCol,
		uniqueIDCol: wk.UniqueIDCol,
		sessCookie:  wk.SessCookie,
		result:      &wk.scrapeWorker.result,
		csvMap:      new(sync.Map),
		reporter:    wk.Reporter,
		wg:          new(sync.WaitGroup),
		log:         wk.log,
	}

	wk.indexToCSVWorker = worker
	worker.run()
}

func (wk *LinkedinScraper) runIndexToJSONWorker() {
	worker := &indexToJSONWorker{
		db:          wk.DB,
		userID:      wk.UserID,
		reqID:       wk.ReqID,
		usernamCol:  wk.UsernamCol,
		uniqueIDCol: wk.UniqueIDCol,
		sessCookie:  wk.SessCookie,
		result:      &wk.scrapeWorker.result,
		csvMap:      new(sync.Map),
		wg:          new(sync.WaitGroup),
		log:         wk.log,
	}

	worker.run()
}

func (wk *LinkedinScraper) Wait() {
	<-wk.exit
}

func (wk *LinkedinScraper) ReadLog() chan [2]string {
	return wk.log
}

func (wk *LinkedinScraper) TerminateOnFinish() bool {
	return true
}

func (wk *LinkedinScraper) CleanUp() error {
	return nil
}

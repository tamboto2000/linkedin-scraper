package controller

import (
	"bytes"
	"compress/gzip"
	"controller"
	"encoding/base64"
	"io/ioutil"
	"linkedin_scraper/app/models"

	"github.com/jinzhu/gorm"
)

func download(ctx *controller.Context) {
	userID := ctx.Request.URL.Query().Get("userID")
	reqID := ctx.Request.URL.Query().Get("requestID")
	ty := ctx.Request.URL.Query().Get("type")

	if userID == "" || reqID == "" || ty == "" {
		ctx.BadRequest("all parameter need to be filled")
		return
	}

	if ty != "json" && ty != "csv" {
		ctx.BadRequest("unrecognized option in 'type' parameter")
		return
	}

	if ty == "csv" {
		data, err := loadCSV(ctx.App.DB, userID, reqID)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not ready yet")
				return
			}

			ctx.InternalError(err.Error())
			return
		}

		ctx.ServeContent("result.zip", data)
	} else {
		data, err := loadJSON(ctx.App.DB, userID, reqID)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not available yet")
				return
			}

			ctx.InternalError(err.Error())
		}

		ctx.ServeContent("result.json", data)
	}
}

func loadCSV(db *gorm.DB, userID, reqID string) ([]byte, error) {
	lnResult := new(models.LinkedinResult)
	if err := db.Take(lnResult, models.LinkedinResult{
		UserID: userID,
		ReqID:  reqID,
	}).Error; err != nil {
		return []byte{}, err
	}

	return base64.StdEncoding.DecodeString(lnResult.Data)
}

func loadJSON(db *gorm.DB, userID, reqID string) ([]byte, error) {
	tweetData := new(models.LinkedinResult)
	if err := db.Take(tweetData, models.LinkedinResult{
		UserID: userID,
		ReqID:  reqID,
		Type:   "json",
	}).Error; err != nil {
		return []byte{}, err
	}

	decData, err := base64.StdEncoding.DecodeString(tweetData.Data)
	if err != nil {
		return []byte{}, err
	}

	//decompress data
	reader, err := gzip.NewReader(bytes.NewReader(decData))
	if err != nil {
		return []byte{}, err
	}

	return ioutil.ReadAll(reader)
}

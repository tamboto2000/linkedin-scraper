package controller

import (
	"collin/spider/util"
	"controller"
	"encoding/base64"
	"encoding/csv"
	"linkedin_scraper/app/misc/reporter"
	"linkedin_scraper/app/worker/linkedin"
	"strings"
)

func startScraper(ctx *controller.Context) {
	ctx.Request.ParseForm()
	userID := ctx.Request.FormValue("userID")
	sessCookie := ctx.Request.FormValue("session")
	csvString := ctx.Request.FormValue("csv")
	lable := ctx.Request.FormValue("lable")
	usernameCol := ctx.Request.FormValue("usernameCol")
	uniqueIDCol := ctx.Request.FormValue("uniqueIDCol")

	if userID == "" || sessCookie == "" || csvString == "" || lable == "" ||
		usernameCol == "" || uniqueIDCol == "" {
		ctx.BadRequest("all parameter need to be filled")
		return
	}

	decodedCSV, err := base64.StdEncoding.DecodeString(csvString)
	if err != nil {
		ctx.BadRequest("invalid csv string for parameter csv")
		return
	}

	csvMap, err := parseCSVToMap(decodedCSV)
	if err != nil {
		ctx.BadRequest(err.Error())
		return
	}

	reqID := util.RandString(20)
	token := util.RandString(40)

	if err := createReporterRoom(reqID, token); err != nil {
		ctx.InternalError(err.Error())
		return
	}

	reporter, err := createReporter(reqID, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	worker := linkedin.New()
	worker.DB = ctx.App.DB
	worker.CSVData = csvMap
	worker.UserID = userID
	worker.ReqID = reqID
	worker.Lable = lable
	worker.UsernamCol = usernameCol
	worker.UniqueIDCol = uniqueIDCol
	worker.SessCookie = sessCookie
	worker.Reporter = reporter

	ctx.App.Worker.Add("linkedin-"+reqID, worker)
	ctx.App.Worker.Run("linkedin-" + reqID)

	ctx.WriteJSON(map[string]interface{}{
		"status":     "OK",
		"statusCode": 200,
		"requestID":  reqID,
		"token":      token,
	})
}

func createReporter(reqID, pass string) (*reporter.Reporter, error) {
	reporter := reporter.NewReporter()
	reporter.ConnID = "linkedin_master"
	reporter.RoomID = "linkedin-" + reqID
	reporter.Password = pass
	reporter.Log = ctrl.Log

	return reporter, reporter.Connect()
}

func createReporterRoom(reqID, pass string) error {
	request := util.NewRequest()
	// request.SetUrl("http://31.220.61.116:8000/createRoom")
	request.SetUrl("http://172.18.0.2:8000/createRoom")
	request.SetQuery("roomID", "linkedin-"+reqID)
	request.SetQuery("roomName", "Linkedin Report Channel")
	request.SetQuery("protected", "true")
	request.SetQuery("password", pass)
	err := request.Execute()
	defer request.Close()

	return err
}

func parseCSVToMap(csvBytes []byte) ([]map[string]string, error) {
	csvReader := csv.NewReader(strings.NewReader(string(csvBytes)))
	csvData, err := csvReader.ReadAll()
	if err != nil {
		return []map[string]string{}, err
	}

	return util.ParseCsvArrayToMap(csvData), nil
}

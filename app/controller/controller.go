package controller

import (
	"controller"
)

var ctrl = controller.NewController()

//Controllers return controller collection, register controllers in this function
func Controllers() *controller.Controller {
	ctrl.Add("startScraper", startScraper)
	ctrl.Add("download", download)
	return ctrl
}
